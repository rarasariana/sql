cd c:\xampp\ysql\bin
cmd: mysql -uroot

1. Membuat Database
   create database myshop;

2. Membuat Table
categories
    create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
items
create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> categories_id int,
    -> primary key(id),
    -> foreign key(categories_id) references categories(id)
    -> );
users
create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. Memasukkan Data pada Tabel
   categories
      insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
   items
      insert into items(name, description, price, stock, categories_id) values ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
   users
      insert into users(name, email, password) values("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");

4. Mengambil Data dari Database
	a. Mengambil data users
	   select * from users;
	   select id, name, email from users;
	b. Mengambil data items
	   select * from items where price > 1000000;
	   select * from items where name like '%watch';
	c. Menampilkan data items join dengan kategori
	   select items.id, items.name, items.description, items.price, items.stock, items.categories_id, categories.name as kategori from items inner join categories on items.categories_id = categories.id;

5. Mengubah Data dari Database
   update items set price = 2500000 where id = 1;
